// animated burger
$(document).ready(function () {
    $('.first-button').on('click', function () {
        $('.animated-icon1').toggleClass('open');
    });
    $('.second-button').on('click', function () {
        $('.animated-icon2').toggleClass('open');
    });
    $('.third-button').on('click', function () {
        $('.animated-icon3').toggleClass('open');
    });
    $('#nav-icon0,#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4,#nav-icon5').click(function () {
        $(this).toggleClass('open');
    });
});

$("button.first-button").click(function () {
    $("nav.navbar").toggleClass("navbar-box-shadow");
});




$(function() {
    $(window).on('load scroll resize orientationchange', function() {
        var scroll = $(window).scrollTop();
        var $win = $(window);
        if (scroll >= 1) {
            $(".navbar").addClass("scroll");
            $(".number-desctop").addClass("scroll");
        } else {
            $(".navbar").removeClass("scroll");
            $(".number-desctop").removeClass("scroll");
        }
    });
});




// var feed = new Instafeed({
//     get: 'user',
//     target: "instafeed-container",
//     accessToken: 'IGQVJVYTNIV3dVWktzN3dtenE5cm5PQ09zWC16bTNWQnRYLTBVdHNIeHRvT0R6M2xPUXpxdWdMTTlLRGN4b2VIcVl3UVpwaDgzUXdzMkh3ckh5U3RXOHBiX2ZAnUFprTk9FaFpNakFISHZATbU5KaTVHTwZDZD',
//     resolution: 'standard_resolution',
//     template: '<div class="swiper-slide"><a href="{{link}}"><span class="img-insta" style="background-image:url({{image}})"></span></a></div>',

//     after: function() {
//         var swiper = new Swiper('.swiper-container-instagram', {
//             loop: true,
//             autoplay: true,
//             delay: 2500,
//             spaceBetween: 15,

//             navigation: {
//                 nextEl: '.swiper-button-next',
//                 prevEl: '.swiper-button-prev',
//             },

//             breakpoints: {
//                 450: {
//                     slidesPerView: 1,
//                     spaceBetween: 20,
//                 },
//                 640: {
//                     slidesPerView: 2,
//                     spaceBetween: 20,
//                 },
//                 768: {
//                     slidesPerView: 3,
//                     spaceBetween: 20,
//                 },
//                 1024: {
//                     slidesPerView: 3,
//                     spaceBetween: 20,
//                 },
//                 1200: {
//                     slidesPerView: 3,
//                     spaceBetween: 20,
//                 },
//                 1665: {
//                     slidesPerView: 4,
//                     spaceBetween: 40,
//                 },
//             }

//         });
//     }
// });
// feed.run();



jQuery(function($) {
    // Bootstrap menu magic
    $(window).resize(function() {
        if ($(window).width() < 768) {
            $(".dropdown-toggle").attr('data-toggle', 'dropdown');
        } else {
            $(".dropdown-toggle").removeAttr('data-toggle dropdown');
        }
    });
});









// Logic inside of function
function addRemoveDiv() {
    // Window Width pointer
    var wW = $(window).width();
    // If window width is greater than or equal to 980 and div not created.
    if (wW >= 1200 && !$('.logo-wrap').length && !$('.menu-wrap').length) {
        $('.navbar-brand').wrap('<div class="logo-wrap"></div>');
        $(".navbar-toggler, #navbarNavDropdown, .second").wrapAll('<div class="menu-wrap"></div>');

        // else if window is less than 980 and #newbox has been created.
    } else if (wW < 1200 && $('.logo-wrap').length && $('.menu-wrap').length) {
        $('.navbar-brand').unwrap();
        $('.navbar-toggler, #navbarNavDropdown').unwrap();
    }
}

// Initial function call.
addRemoveDiv();

// On resize, call the function again
$(window).on('resize', function() {
    addRemoveDiv();
})









var swiper = new Swiper('.swiper-container-main', {
    spaceBetween: 30,
    loop: true,
    autoplay: true,
    delay: 3000,

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    breakpoints: {
        450: {
            slidesPerView: 1,
            spaceBetween: 20,
        },
        640: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        1200: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        1665: {
            slidesPerView: 4,
            spaceBetween: 40,
        },
    }

});



var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 15,
    slidesPerView: 3,
    direction: 'horizontal',
    loop: false,
    freeMode: true,
    loopedSlides: 2, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    breakpoints: {
        991: {
            direction: 'vertical',
        },

    }
});
var galleryTop = new Swiper('.gallery-top', {
    loop: true,
    loopedSlides: 1, //looped slides should be the same
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: galleryThumbs,
    },
});




$(function() {
    $(window).on('load scroll resize orientationchange', function() {
        var scroll = $(window).scrollTop();
        var $win = $(window);
        if (scroll >= 1) {
            $(".logo-mobile img").addClass("scroll");
            $(".navbar-top").addClass("scroll");
        } else {
            $(".logo-mobile img").removeClass("scroll");
            $(".navbar-top").removeClass("scroll");
        }
    });
});








