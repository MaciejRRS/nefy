<?php 
/* 
Template Name: Poradnik 
*/ 
?>

<?php get_header() ?>

<div id="poradnik">
    <div class="poradnik-wrapper">
        <div class="container">
            <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        } ?>
            <div class="title-section">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="row">
                <div class="col-lg-2">
                    <div class="column-filter">
                        <?php echo do_shortcode('[searchandfilter id="402"]') ?>
                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="poradnik-content-top">


                        <?php

$custom_query = new WP_Query( 
    array(
    'post_type' => 'recipes',
    'post_status'=>'publish',
    'posts_per_page' => 1,
    ) 
); ?>



                        <?php if ( $custom_query->have_posts() ) : while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
                        <a href="<?php the_permalink(); ?>">
                            <div class="row">
                                <div class="col-lg-6">

                                    <div style="background-image: url(<?php the_field('zdjecie_glowne_poradnik_head') ?>)"
                                        class="left-col-img"></div>
                                </div>
                                <div class="col-lg-6 right-col-text-apla">


                                    <?php 
                             
                             $terms = get_the_terms($post->ID, 'category_recipes');
                             foreach($terms as $term) {
                             $category_recipes = $term->name;                             
                             
                             if  ($category_recipes == 'Natural') { ?>
                                    <div class="poradnik-header-text">
                                        <h2>
                                            <?php the_title(); ?>
                                        </h2>
                                    </div>
                                    <?php }
else {
    if  ($category_recipes == 'Professional') { ?>
                                    <div class="poradnik-header-text color-professional-line">
                                        <h2>
                                            <?php the_title(); ?>
                                        </h2>
                                    </div>
                                    <?php } 
}?>


                                    <?php 
 if  ($category_recipes == 'Natural') { ?>
                                    <div class="greenApla-rightCol">
                                        <?php }
else {
    if  ($category_recipes == 'Professional') { ?>
                                        <div class="greenApla-rightCol redAplaProffesional">
                                            <?php } 
}
?>






                                            <div class="container-text-default">
                                                <p><?php echo mb_strimwidth( get_the_excerpt(), 0, 200, '...' ); ?>
                                                </p>
                                            </div>

                                        </div>




                                        <div class="logo-product-category">

                                            <img
                                                src="<?php the_field('miniatura_kategorii_custom_post_natural_professional', $term); ?>" />
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                        </a>

                        <?php
endwhile; 
endif; wp_reset_query();
?>



                    </div>
                    <div class="products-list">
                        <?php echo do_shortcode('[searchandfilter id="402" show="results"]') ?>
                    </div>
                </div>
            </div>



        </div>
    </div>

</div>



































<?php get_footer() ?>