<?php get_header() ?>

<div id="poradnik-single">
    <div class="poradnik-wrapper">
        <div class="container">
            <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        } ?>
            <div class="poradnik-content-top">
                <div class="row">
                    <div class="col-lg-7">
                        <!-- <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                        <div class="left-col-img" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
                        </div> -->
                        <div class="left-col-img"
                            style="background-image: url(<?php the_field('zdjecie_glowne_poradnik_head') ?>)">
                        </div>

                    </div>
                    <div class="col-lg-5 block-right-position">
                        <div class="poradnik-header-text">
                            <h1>
                                <?php the_title(); ?>
                            </h1>
                        </div>
                        <div class="dataPost-poradnik">
                            <?php the_time('d.m.Y') ?>
                        </div>

                        <?php
        $terms = get_the_terms($post->ID, 'category_recipes');
        foreach($terms as $term) {
        $category_recipes = $term->name;
        }
        if  ($category_recipes == 'Natural') { ?>

                        <div class="greenApla-rightCol">
                            <div class="container-text-default">
                                <p><?php echo mb_strimwidth( get_the_excerpt(), 0, 220, '...' ); ?></p>
                            </div>
                        </div>
                        <?php }
        else {
            if  ($category_recipes == 'Professional') { ?>
                        <div class="purpleApla-rightCol">
                            <div class="container-text-default">
                                <p><?php echo mb_strimwidth( get_the_excerpt(), 0, 220, '...' ); ?></p>
                            </div>
                        </div>
                        <?php  } 
        }
    ?>
                    </div>
                </div>
            </div>

            <div class="content-single-poradnik">
                <div class="row">
                    <div class="col-lg-9">
                        <?php the_content(); ?>
                    </div>
                    <div class="col-lg-3">

                        <?php if( get_field('zdjecie_produktu_w_tresci_poradnik') ): ?>

                        <div class="img-product-post-poradnik">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/pic-single-poradnik.svg"
                                class="pic-single-poradnik">
                            <img class="img-zdjecie_produktu_w_tresci_poradnik"
                                src="<?php the_field('zdjecie_produktu_w_tresci_poradnik') ?>"
                                alt="<?php the_title() ?>">
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>