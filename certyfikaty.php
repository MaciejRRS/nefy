<?php 
/* 
Template Name: Certyfikaty
*/ 
?>

<?php get_header() ?>

<div id="subpageDefault">
    <div class="subpage-wrapper">
        <div class="container">

            <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
    } ?>

            <div class="title-section">
                <h1><?php the_title(); ?></h1>
            </div>

            <?php
if( have_rows('lista_certyfikatow') ):
    while( have_rows('lista_certyfikatow') ) : the_row(); ?>
            <div class="cert-content nth-cols">
                <div class="row">
                    <div class="col-lg-5 col-order-one">
                        <div class="bg-cover">
                            <a data-fancybox="gallery" href="<?php the_sub_field('img_certyficate') ?>"><img
                                    class="img-cover-cert" src="<?php the_sub_field('img_certyficate') ?>"
                                    alt="<?php the_sub_field('name_certyficate') ?>"></a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-order-two">
                        <div class="text-certyficate">
                            <h2><?php the_sub_field('name_certyficate') ?></h2>
                            <?php the_sub_field('textDesc_certyficate') ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php 
endwhile;
else :
endif; ?>
        </div>
    </div>
</div>



































<?php get_footer() ?>