<?php

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';



function add_stylesheets_and_scripts()
{
wp_enqueue_style( 'style', get_stylesheet_uri(), array(), filemtime(get_template_directory() . '/style.css'), 'all' );
wp_enqueue_style('custom', get_template_directory_uri() . '/assets/dist/css/main.min.css', array(), filemtime(get_template_directory() . '/assets/dist/css/main.min.css'), 'all' );

wp_deregister_script('jquery');
wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/src/js/libr/jquery.min.js', array(), null, true);
wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/src/js/libr/bootstrap.min.js', array(), null, true);
// wp_enqueue_script('popper', get_template_directory_uri() . '/assets/src/js/libr/popper.min.js', array(), null, true);
wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/src/js/main.js', array(), null, true );
wp_enqueue_script( 'fontawesome', get_template_directory_uri() . '/assets/src/js/fontawesome.js', array(), null, true );

wp_enqueue_style( 'style-swiper', get_template_directory_uri() . '/assets/src/css/swiper-bundle.css' );
// wp_enqueue_script( 'js-swiper', get_template_directory_uri() . '/assets/src/js/swiper-bundle.min', array(), null, true );
// wp_enqueue_script( 'js-instafeed', get_template_directory_uri() . '/assets/src/js/instafeed.min.js', array(), null, true );

    wp_enqueue_style( 'style-fancybox', get_template_directory_uri() . '/assets/src/css/jquery.fancybox.min.css' );
    wp_enqueue_script( 'js-fancybox', get_template_directory_uri() . '/assets/src/js/jquery.fancybox.min.js', array(), null, true );





}
add_action('wp_enqueue_scripts', 'add_stylesheets_and_scripts');




register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'tsl' ),
    'secondary' => __( 'Secondary Menu', 'tsl' ),
	'secondary_two' => __( 'social icon menu', 'tsl' ),
	'secondary_three' => __( 'Language menu', 'tsl' ),
	'secondary_four' => __( 'Search menu', 'tsl' ),
) );


// register sidebar
function enable_widgets() {

    register_sidebar(
        array(
            'name' => 'Main Sidebar',
            'id'   => 'sidebar-1',
            'description'   => 'Here you can add widgets to the main sidebar.',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h5 id="widget-heading">',
            'after_title'   => '</h5>'
    ));
 }

 add_action('widgets_init','enable_widgets');





 /**
 * Change several of the breadcrumb defaults
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => ' &gt; ',
            'wrap_before' => '<nav class="woocommerce-breadcrumb" itemprop="breadcrumb">',
            'wrap_after'  => '</nav>',
            'before'      => '',
            'after'       => '',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
        );
}






/*
* Enable support for custom logo.
*
*/
	add_theme_support( 'custom-logo', array(
		'height'      => 400,
		'width'       => 100,
		'flex-height' => true,
		'flex-width'  => true,
	) );



    add_filter('wpcf7_autop_or_not', '__return_false');





// Register thumbnails
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'homepage-thumb', 385, 302 ); // Soft Crop Mode





// This adds support for pages only:
	add_theme_support( 'post-thumbnails', array( 'page' ) );

//add option page to panel (ACF)
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();

}






// // productY
// // Dodawanie nowego typu postów START
// // add the filter
// // add_filter( 'acf_the_content', 'filter_acf_the_content', 10, 1 );

function create_product_posttype(){
	$args = array(
	'label' => 'produkty',
	'labels' => array(
	'name' => __('produkty'),
	'singular_name' => __('produkty'),
	'add_new' => __('Dodaj produkt'),
	'add_new_item' => __('Dodaj Nowy produkt'),
	'edit_item' => __('Edytuj produkt'),
	'new_item' => __('Nowy produkt'),
	'view_item' => __('Zobacz produkt'),
	'search_items' => __('Szukaj produkt'),
	'not_found' => __('Nie Znaleziono produkt'),
	'not_found_in_trash' => __('Brak produkt w koszu'),
	'all_items' => __('Wszystkie produkt'),
	'archives' => __('Zarchiwizowane producty'),
	'insert_into_item' => __('Dodaj do produktu'),
	'uploaded_to_this_item' => __('Sciągnięto do bieżącego produktu'),
	'featured_image' => __('Zdjęcie productu'),
	'set_featured_image' => __('Ustaw Zdjęcie produktu'),
	'remove_featured_image' => __('Usuń Zdjęcie produktu'),
	'use_featured_image' => __('Użyj Zdjęcie produktu'),
	'menu_name' => __('produkty')
	),
	'description' => __('Typ Postu zawiera treść dla produktu'),
	'public' => true,
	'exclude_from_search' => false,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_nav_menus' => true,
	'show_in_menu' => true,
	'show_in_admin_bar' => true,
	'menu_position' => 5,
	'menu_icon' => 'dashicons-products',
	'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes'),
	'has_archive' => false,
	'hierarchical' => false,
	'show_in_rest' => true,
	'rewrite' => array('slug'=>'product','with_front'=>false),
	'capabilities' => array(
	'edit_post' => 'update_core',
	'read_post' => 'update_core',
	'delete_post' => 'update_core',
	'edit_posts' => 'update_core',
	'edit_others_posts' => 'update_core',
	'delete_posts' => 'update_core',
	'publish_posts' => 'update_core',
	'read_private_posts' => 'update_core'
	),
	);
	register_post_type('product',$args);
	}
	add_action('init','create_product_posttype',0);
	
	function register_taxonomy_category_product() {
	$labels = [
	'name' => _x('Categories', 'taxonomy general name'),
	'singular_name' => _x('Category', 'taxonomy singular name'),
	'search_items' => __('Search Categories'),
	'all_items' => __('All Categories'),
	'parent_item' => __('Parent Category'),
	'parent_item_colon' => __('Parent Category:'),
	'edit_item' => __('Edit Category'),
	'update_item' => __('Update Category'),
	'add_new_item' => __('Add New Category'),
	'new_item_name' => __('New Category Name'),
	'menu_name' => __('Kategorie'),
	];
	$args = [
	'hierarchical' => true, // make it hierarchical (like categories)
	'labels' => $labels,
	'show_ui' => true,
	'show_in_rest' => true,
	'show_admin_column' => true,
	'query_var' => true,
	'rewrite' => ['slug' => 'category_product'],
	];
	register_taxonomy('category_product', ['product'], $args);
	}
	add_action('init', 'register_taxonomy_category_product');
	







// ponieszczenia
	function register_taxonomy_category_product_room() {
		$labels = [
			'name'              => _x('Categories', 'taxonomy general name'),
			'singular_name'     => _x('Category', 'taxonomy singular name'),
			'search_items'      => __('Search Categories'),
			'all_items'         => __('All Categories'),
			'parent_item'       => __('Parent Category'),
			'parent_item_colon' => __('Parent Category:'),
			'edit_item'         => __('Edit Category'),
			'update_item'       => __('Update Category'),
			'add_new_item'      => __('Dodaj nowe pomieszczenie'),
			'new_item_name'     => __('New Category Name'),
			'menu_name'         => __('Pomieszczenia'),
			];
			$args = [
			'hierarchical'      => true, // make it hierarchical (like categories)
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_rest' 		=> true,
			'query_var'         => true,
			'has_archive'		=> true,
			'rewrite'           => ['slug' => 'category_product_room',
									'hierarchical' => true,
									'with_front' => true],
			];
			register_taxonomy('category_product_room', array('product'), $args);
	}
	add_action('init', 'register_taxonomy_category_product_room');
	
	


// Zastosowanie
function register_taxonomy_category_product_application() {
	$labels = [
		'name'              => _x('Categories', 'taxonomy general name'),
		'singular_name'     => _x('Category', 'taxonomy singular name'),
		'search_items'      => __('Search Categories'),
		'all_items'         => __('All Categories'),
		'parent_item'       => __('Parent Category'),
		'parent_item_colon' => __('Parent Category:'),
		'edit_item'         => __('Edit Category'),
		'update_item'       => __('Update Category'),
		'add_new_item'      => __('Dodaj nowe zastosowanie'),
		'new_item_name'     => __('New Category Name'),
		'menu_name'         => __('Zastosowania'),
		];
		$args = [
		'hierarchical'      => true, // make it hierarchical (like categories)
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest' 		=> true,
		'query_var'         => true,
		'has_archive'		=> true,
		'rewrite'           => ['slug' => 'category_product_application',
								'hierarchical' => true,
								'with_front' => true],
		];
		register_taxonomy('category_product_application', array('product'), $args);
}
add_action('init', 'register_taxonomy_category_product_application');
	
	
	
	
	
	
	
	
	
	
	
	
	
	
// // recipes
// // Dodawanie nowego typu postów START
function create_recipes_posttype(){
	$args = array(
	'label' => 'poradnik',
	'labels' => array(
	'name' => __('poradnik'),
	'singular_name' => __('poradnik'),
	'add_new' => __('Dodaj poradnik'),
	'add_new_item' => __('Dodaj Nowy poradnik'),
	'edit_item' => __('Edytuj poradnik'),
	'new_item' => __('Nowy poradnik'),
	'view_item' => __('Zobacz poradnik'),
	'search_items' => __('Szukaj poradnik'),
	'not_found' => __('Nie Znaleziono poradnika'),
	'not_found_in_trash' => __('Brak poradnika w koszu'),
	'all_items' => __('Wszystkie poradniki'),
	'archives' => __('Zarchiwizowane poradniki'),
	'insert_into_item' => __('Dodaj do poradnika'),
	'uploaded_to_this_item' => __('Sciągnięto do bieżącego poradnika'),
	'featured_image' => __('Zdjęcie poradnika'),
	'set_featured_image' => __('Ustaw Zdjęcie poradnika'),
	'remove_featured_image' => __('Usuń Zdjęcie poradnika'),
	'use_featured_image' => __('Użyj Zdjęcie poradnika'),
	'menu_name' => __('poradnik')
	),
	'description' => __('Typ Postu zawiera treść dla Pradnika'),
	'public' => true,
	'exclude_from_search' => false,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_nav_menus' => true,
	'show_in_menu' => true,
	'show_in_admin_bar' => true,
	'menu_position' => 5,
	'menu_icon' => 'dashicons-coffee',
	'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes'),
	'has_archive' => false,
	'hierarchical' => false,
	'show_in_rest' => true,
	'rewrite' => array('slug'=>'recipes','with_front'=>false),
	'capabilities' => array(
	'edit_post' => 'update_core',
	'read_post' => 'update_core',
	'delete_post' => 'update_core',
	'edit_posts' => 'update_core',
	'edit_others_posts' => 'update_core',
	'delete_posts' => 'update_core',
	'publish_posts' => 'update_core',
	'read_private_posts' => 'update_core'
	),
	);
	register_post_type('recipes',$args);
	}
	add_action('init','create_recipes_posttype',0);
	
	function register_taxonomy_category_recipes() {
	$labels = [
	'name' => _x('Categories', 'taxonomy general name'),
	'singular_name' => _x('Category', 'taxonomy singular name'),
	'search_items' => __('Search Categories'),
	'all_items' => __('All Categories'),
	'parent_item' => __('Parent Category'),
	'parent_item_colon' => __('Parent Category:'),
	'edit_item' => __('Edit Category'),
	'update_item' => __('Update Category'),
	'add_new_item' => __('Add New Category'),
	'new_item_name' => __('New Category Name'),
	'menu_name' => __('Kategorie'),
	];
	$args = [
	'hierarchical' => true, // make it hierarchical (like categories)
	'labels' => $labels,
	'show_ui' => true,
	'show_in_rest' => true,
	'show_admin_column' => true,
	'query_var' => true,
	'rewrite' => ['slug' => 'category_recipes'],
	];
	register_taxonomy('category_recipes', ['recipes'], $args);
	}
	add_action('init', 'register_taxonomy_category_recipes');	
	
	
	// ponieszczenia dla recipesa
	function register_taxonomy_category_recipes_room() {
		$labels = [
			'name'              => _x('Categories', 'taxonomy general name'),
			'singular_name'     => _x('Category', 'taxonomy singular name'),
			'search_items'      => __('Search Categories'),
			'all_items'         => __('All Categories'),
			'parent_item'       => __('Parent Category'),
			'parent_item_colon' => __('Parent Category:'),
			'edit_item'         => __('Edit Category'),
			'update_item'       => __('Update Category'),
			'add_new_item'      => __('Dodaj nowe pomieszczenie'),
			'new_item_name'     => __('New Category Name'),
			'menu_name'         => __('Pomieszczenia'),
			];
			$args = [
			'hierarchical'      => true, // make it hierarchical (like categories)
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_rest' 		=> true,
			'query_var'         => true,
			'has_archive'		=> true,
			'rewrite'           => ['slug' => 'category_recipes_room',
									'hierarchical' => true,
									'with_front' => true],
			];
			register_taxonomy('category_recipes_room', array('recipes'), $args);
	}
	add_action('init', 'register_taxonomy_category_recipes_room');
	
	
	
	
	// Zastosowanie dla recipesa
function register_taxonomy_category_recipes_application() {
	$labels = [
		'name'              => _x('Categories', 'taxonomy general name'),
		'singular_name'     => _x('Category', 'taxonomy singular name'),
		'search_items'      => __('Search Categories'),
		'all_items'         => __('All Categories'),
		'parent_item'       => __('Parent Category'),
		'parent_item_colon' => __('Parent Category:'),
		'edit_item'         => __('Edit Category'),
		'update_item'       => __('Update Category'),
		'add_new_item'      => __('Dodaj nowe zastosowanie'),
		'new_item_name'     => __('New Category Name'),
		'menu_name'         => __('Zastosowania'),
		];
		$args = [
		'hierarchical'      => true, // make it hierarchical (like categories)
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest' 		=> true,
		'query_var'         => true,
		'has_archive'		=> true,
		'rewrite'           => ['slug' => 'category_recipes_application',
								'hierarchical' => true,
								'with_front' => true],
		];
		register_taxonomy('category_recipes_application', array('recipes'), $args);
}
add_action('init', 'register_taxonomy_category_recipes_application');
	






// contact form 7 only on specific pages
add_action( 'wp_print_scripts', 'deregister_cf7_javascript', 100 );
function deregister_cf7_javascript() {
if ( !is_page(array(15)) ) {
wp_deregister_script( 'contact-form-7' );
wp_deregister_script('google-recaptcha');
}
}
add_action( 'wp_print_styles', 'deregister_cf7_styles', 100 );
function deregister_cf7_styles() {
if ( !is_page(array(15)) ) {
wp_deregister_style( 'contact-form-7' );
}
}
	
	?>