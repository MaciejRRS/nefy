<?php
/**
 * The template for displaying Search Results pages.
 */

get_header(); ?>


<div id="subpageDefault">
    <div class="subpage-wrapper">
        <div class="container">

            <?php if ( have_posts() ) : ?>

            <header class="page-header-search">
                <h2 class="searchTitle">
                    <?php printf( the_field('resultaty_wyszukiwania_tekst', 'option' ) , '<span>' . get_search_query() . '</span>' ); ?>
                    <?php printf( __( '%s', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?>

                </h2>
            </header>

            <?php /* Start the Loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>
            <ul class="search-list">
                <li>
                    <i class="fas fa-search"></i><a class="search-link-item"
                        href="<?php the_permalink(); ?>"><?php echo wp_trim_words( get_the_title(), 10, '...' ); ?></a>
                </li>
            </ul>

            <?php endwhile; ?>

            <a href="/" class="return d-block mx-auto text-center"><i class="fas fa-angle-left"></i>
                <?php the_field('tekst_linku_powrot', 'option' ) ?></a>

            <!-- blok tekstu -->

            <?php else : ?>

            <header class="page-header-search">

                <h2 class="text-center"><?php the_field('brak_rezultatow_wyszukiwania_tekst', 'option' ) ?>
                </h2>
                <a href="/" class="return d-block mx-auto text-center"><i class="fas fa-angle-left"></i>
                    <?php the_field('tekst_linku_powrot', 'option' ) ?></a>

            </header>

            <?php endif; ?>

        </div><!-- #container-->
        </section><!-- search-area -->


    </div>
</div>




<?php get_footer(); ?>