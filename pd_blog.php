<?php
/*
 Template Name: Aktualności
 */
?>
<?php
get_header(); ?>
<main id="poradnik">
    <div class="poradnik-wrapper">
        <div class="container">
            <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        } ?>
            <div class="title-section">
                <h1><?php the_title(); ?></h1>
            </div>




            <?php
// Protect against arbitrary paged values
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;


 
$args = array(
    'post_type' => 'post',
    'post_status'=>'publish',
    'paged' => $paged,
);

$the_query = new WP_Query($args);
?>

            <?php if ( $the_query->have_posts() ) : ?>


            <section class="news">
                <div class="blocks-news-area">
                    <div class="row">

                        <!-- the loop -->
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                        <!-- start post block -->
                        <div class="col-md-6">
                            <a href="<?php the_permalink(); ?>">
                                <div class="poradnik-post-area news">

                                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                                    <div class="img-productArea"
                                        style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>

                                    <div class="descProduct_item">
                                        <div class="date_post">
                                            <?php echo get_the_date(); ?>
                                        </div>
                                        <?php echo '<h3 class="natural_color_line">'.get_the_title().'</h3>';
                   
                ?>
                                        <div class="descApla">
                                            <p><?php echo mb_strimwidth( get_the_excerpt(), 0, 150, '...' ); ?></p>
                                        </div>


                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- end block post -->
















                        <?php endwhile; ?>
                        <!-- end of the loop -->

                        <?php
wp_reset_query();
?>




                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pagination">
                            <?php
								echo paginate_links( array(
									'format'  => 'page/%#%',
									'current' => $paged,
									'total'   => $the_query->max_num_pages,
									'mid_size'        => 2,
									'prev_text'       => __('&laquo;  Cofnij'),
									'next_text'       => __('Dalej  &raquo;')
								) );
							?>
                        </div>

                        <?php endif; ?>
                    </div>
                </div>

            </section>








        </div>
    </div>


</main><!-- .site-main -->
<?php get_footer(); ?>