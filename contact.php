<?php 
/* 
Template Name: Kontakt
*/ 
?>
<?php get_header()?>

<main id="subpageDefault">
    <div class="subpage-wrapper">
        <div class="container">

            <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
    } ?>
            <div class="title-section">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="contact-wrapper">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="left-contact">
                            <?php the_field('text_contact_information') ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="right-contact-form">
                            <?php echo do_shortcode(get_field('formularz_kontaktowy_page_contact')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>












































<?php get_footer() ?>