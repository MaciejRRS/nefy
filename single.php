<?php get_header() ?>

<div id="poradnik-single">
    <div class="poradnik-wrapper">
        <div class="container">
            <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        } ?>
            <div class="poradnik-content-top">
                <div class="row">
                    <div class="col-lg-7">
                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                        <div class="left-col-img" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
                        </div>
                    </div>
                    <div class="col-lg-5 block-right-position">
                        <div class="poradnik-header-text">
                            <h1>
                                <?php the_title(); ?>
                            </h1>
                        </div>
                        <div class="dataPost-poradnik">
                            <?php the_time('d.m.Y') ?>
                        </div>


                        <div class="greenApla-rightCol grayApla-news">
                            <div class="container-text-default">
                                <p><?php echo mb_strimwidth( get_the_excerpt(), 0, 220, '...' ); ?></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="content-single-poradnik">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>