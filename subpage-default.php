<?php 
/* 
Template Name: Strona tekstowa
*/ 
?>

<?php get_header() ?>

<div id="subpageDefault">
    <div class="subpage-wrapper">
        <div class="container">

            <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
    } ?>

            <div class="title-section">
                <h1><?php the_title(); ?></h1>
            </div>

            <?php while (have_posts()) : the_post();?>
            <div class="container-text-default">
                <?php the_content(); ?>
            </div>
            <?php endwhile; ?>
        </div>
    </div>

</div>



































<?php get_footer() ?>