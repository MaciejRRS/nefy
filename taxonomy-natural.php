<?php get_header() ?>

<div id="produkt">
    <section class="product-top">
        <div class="container">
            <div class="content-breadcrumps">
                <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
    } ?>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="gallery-product">
                        <?php if ( has_post_thumbnail() ) {
                                    the_post_thumbnail();
                                } ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="desc-product-area">
                        <div class="title-product">
                            <h1><?php the_title(); ?></h1>
                            <div class="desc-primary-product">
                                <?php if (have_posts()) : ?>
                                <?php while (have_posts()) : the_post(); ?>
                                <?php the_content();?>
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="desc-product-1">
                            <div class="icon-text-area">
                                <img src="<?php the_field('icon_zastosowanie_product') ?>">
                                <h3><?php the_field('tekst_tytulu_-_zastosowanie_product') ?></h3>
                            </div>
                            <?php the_field('opis_zastosowanie_product') ?>
                        </div>
                        <div class="desc-product-2">
                            <div class="icon-text-area">
                                <img src="<?php the_field('ikona_do_czego_sie_nadaje_product') ?>">
                                <h3><?php the_field('tytul_do_czego_sie_nadaje_product') ?></h3>
                            </div>
                            <div class="TwoCols-text-desc">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="text-left-col">
                                            <?php the_field('gotowe_srodki_czystosci_lewa_col') ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-right-col">
                                            <?php the_field('gotowe_srodki_czystosci_prawa_col') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ilosc_area_field">
                                <?php the_field('iloscArea_col') ?>
                            </div>
                            <div class="buttons-product-area">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <a class="btn-kup-teraz"
                                            href="<?php the_field('przycisk_-_link_kupTeraz') ?>"><?php the_field('przycisk_-_tekst_kupTeraz') ?></a>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="accordion" id="accordionExample">
                                            <div class="card">
                                                <div class="card-header" id="headingOne">

                                                    <button class="btn-do-pobrania" type="button" data-toggle="collapse"
                                                        data-target="#collapseOne" aria-expanded="true"
                                                        aria-controls="collapseOne">
                                                        <?php the_field('przycisk_-_tekst_doPobrania') ?>
                                                    </button>

                                                </div>

                                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                                    data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        <ul>
                                                            <?php
                                                            if( have_rows('list-attachment-product') ):
                                                                while( have_rows('list-attachment-product') ) : the_row(); ?>
                                                            <li><a
                                                                    href="<?php the_sub_field('link_attachment-product'); ?>">
                                                                    <?php the_sub_field('img-attachment-product'); ?>
                                                                    <?php the_sub_field('name_attachment-product'); ?>
                                                                </a>
                                                            </li>

                                                            <?php endwhile;
                                                            else :
                                                            endif;
                                                            ?>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="product-single-apla">
        <div class="container">
            <div class="row fle-center-apla-primary">
                <?php
if( have_rows('lista_ikona_na_apli_produktu') ):
    while( have_rows('lista_ikona_na_apli_produktu') ) : the_row(); ?>

                <div class="col-lg-4 ">
                    <div class="center-iconText-apla-product">
                        <div class="area_icons_Apla">
                            <img src="<?php the_sub_field('ikona_na_apli_produktu'); ?>">
                        </div>
                        <div class="text-icons_Apla">
                            <h3><?php the_sub_field('textPodikona_na_apli_produktu'); ?></h3>
                        </div>
                    </div>
                </div>
                <?php    endwhile;
else :
endif; ?>
            </div>
        </div>
    </section>




    <!-- start new products -->
    <section class="productsNew">
        <div class="container">
            <div class="title-section">
                <h2><?php the_field('title_section_lineAll') ?></h2>
            </div>
            <!-- Swiper -->
            <div class="swiper-container swiper-container-main">
                <div class="swiper-wrapper">
                    <?php
$args = array(
    'post_type' => 'produkt',
    'posts_per_page' => -1,
    'orderby' => 'menu_order',
    'order' => 'ASC'
 );






$parent = new WP_Query( $args );
if ( $parent->have_posts() ) : ?>
                    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

                    <?php if( get_field('product_new_onHome') == 'enable_sidebar' ) { ?>

                    <div class="swiper-slide">
                        <a href="<?php the_permalink(); ?>">
                            <div class="product-area">
                                <div class="img-productArea">
                                    <?php if ( has_post_thumbnail() ) {
                                    the_post_thumbnail();
                                } ?>
                                </div>

                                <?php  
 if( get_field('img_product_back') ) { ?>
                                <div class="img-productArea-back">
                                    <img src="<?php the_field('img_product_back') ?>" alt="<?php the_title(); ?>">
                                </div>
                                <?php } else { ?>
                                <div class="img-productArea-back">
                                    <?php if ( has_post_thumbnail() ) {
  the_post_thumbnail();
} ?>
                                </div>

                                <?php }?>

                                <div class="descProduct_item">
                                    <p><?php echo mb_strimwidth( get_the_title(), 0, 50, '...' ); ?></p>
                                    <p><?php echo mb_strimwidth( get_the_excerpt(), 0, 100, '...' ); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                    <?php endwhile; ?>
                    <?php endif; 
wp_reset_postdata(); ?>

                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </section>
    <!-- end new products -->
</div>

<?php get_footer() ?>