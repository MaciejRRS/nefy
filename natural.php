<?php 
/* 
Template Name: Produkty Natural - filtr         
*/ 
?>

<?php get_header() ?>

<div id="Natural-Profesional-product">
    <div class="page-wrapper">
        <div class="container">
            <!-- <section style="background-image: url(<?php the_field('slider-Natural-Profesional-product') ?>)"
                class="slider-Natural-Profesional-product">
            </section> -->
            <img src="<?php the_field('slider-Natural-Profesional-product') ?>" class="top-slider">
        </div>
        <div class="container">
            <section class="content-Natural-Profesional-product">
                <div class="row">
                    <div class="col-lg-3">
                        <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
    } ?>


                        <div class="column-filter">
                            <?php echo do_shortcode('[searchandfilter id="204"]') ?>
                        </div>


                        <div class="column-filter-img">
                            <a href="<?php the_field('column-filter-img-Natural-Profesional-product-page_link') ?>"><img
                                    src="<?php the_field('column-filter-img-Natural-Profesional-product-page') ?>"
                                    alt="<?php the_field('title-Natural-Profesional-product-page') ?>"></a>
                        </div>

                    </div>
                    <div class="col-lg-9">
                        <div class="title-section">
                            <h1><?php the_field('title-Natural-Profesional-product-page') ?></h1>
                        </div>
                        <div class="products-list">
                            <?php echo do_shortcode('[searchandfilter id="204" show="results"]') ?>
                        </div>

                    </div>
                </div>
            </section>

        </div>
    </div>

</div>



































<?php get_footer() ?>