<?php get_header('colorsProdukt') ?>

<div id="produkt">
    <section class="product-top">
        <div class="container">


            <?php
        $terms = get_the_terms($post->ID, 'category_product');
        foreach($terms as $term) {
        $category_product = $term->name;
        }
        if  ($category_product == 'Natural') { ?>
            <div class="content-breadcrumps natural-colors-breadcrumbs">
                <?php }
        else {
            if  ($category_product == 'Professional') {  ?>
                <div class="content-breadcrumps professional-colors-breadcrumbs">
                    <?php  } 
        }
    ?>









                    <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
    } ?>










                </div>
                <div class="row">
                    <div class="col-lg-7">
                        <div class="gallery-product">
                            <div class="swiper-container gallery-thumbs">
                                <div class="swiper-wrapper">
                                    <?php
                                if( have_rows('galeria_produktu_lista_zdjec_produktu') ):
                                    while( have_rows('galeria_produktu_lista_zdjec_produktu') ) : the_row(); ?>
                                    <img class="thumb-img-gallery-product swiper-slide"
                                        src="<?php the_sub_field('zdjecie_produktu_w_galerii'); ?>"
                                        alt="<?php the_title(); ?>">
                                    <?php    endwhile;
                                else :
                                endif; ?>
                                </div>
                            </div>
                            <div class="swiper-container gallery-top">

                                <div class="swiper-wrapper">
                                    <?php
                                if( have_rows('galeria_produktu_lista_zdjec_produktu') ):
                                    while( have_rows('galeria_produktu_lista_zdjec_produktu') ) : the_row(); ?>
                                    <div class="swiper-slide">
                                        <a data-fancybox="gallery"
                                            href="<?php the_sub_field('zdjecie_produktu_w_galerii'); ?>"><img
                                                src="<?php the_sub_field('zdjecie_produktu_w_galerii'); ?>"
                                                alt="<?php the_sub_field('zdjecie_produktu_-tekst_alternatywny_dla_zdjecia'); ?>">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/zoom-icon.svg"
                                                class="zoom-icon">
                                        </a>
                                    </div>
                                    <?php    endwhile;
                                else :
                                endif; ?>
                                </div>
                                <!-- Add Arrows -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="desc-product-area">

                            <?php
        $terms = get_the_terms($post->ID, 'category_product');
        foreach($terms as $term) {
        $category_product = $term->name;
        }
        if  ($category_product == 'Natural') { ?>
                            <img class="pictogram-naturalORprofessional"
                                src="<?php echo get_template_directory_uri(); ?>/assets/img/lisc_natural.svg">
                            <?php }
        else {
            if  ($category_product == 'Professional') {  ?>
                            <!-- <img class="pictogram-naturalORprofessional"
                                src="<?php echo get_template_directory_uri(); ?>/assets/img/pictogram_professional.svg"> -->
                            <?php  } 
        }
    ?>



                            <!-- <img class="pictogram-naturalORprofessional"
                            src="<?php echo get_template_directory_uri(); ?>/assets/img/lisc_natural.svg"> -->
                            <div class="title-product">
                                <h1><?php the_title(); ?></h1>
                                <div class="desc-primary-product">
                                    <?php if (have_posts()) : ?>
                                    <?php while (have_posts()) : the_post(); ?>
                                    <?php the_content();?>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="desc-product-1">

                                <!-- 
                            <?php
        $terms = get_the_terms($post->ID, 'category_product');
        foreach($terms as $term) {
        $category_product = $term->name;
        }
        if  ($category_product == 'Natural') { ?>
                            <div class="icon-text-area color-natural">
                                <h3><?php the_field('tekst_tytulu_-_zastosowanie_product2') ?></h3>
                            </div>
                            <?php }
        else {
            if  ($category_product == 'Professional') {  ?>
                            <div class="icon-text-area color-professional">
                                <h3><?php the_field('tekst_tytulu_-_zastosowanie_product2') ?></h3>
                            </div>
                            <?php  } 
        }
    ?> -->



                                <?php the_field('opis_zastosowanie_product2') ?>
                            </div>
                            <div class="desc-product-1">

                                <?php
        $terms = get_the_terms($post->ID, 'category_product');
        foreach($terms as $term) {
        $category_product = $term->name;
        }
        if  ($category_product == 'Natural') { ?>
                                <div class="icon-text-area color-natural">
                                    <img src="<?php the_field('icon_zastosowanie_product') ?>">
                                    <h3><?php the_field('tekst_tytulu_-_zastosowanie_product') ?></h3>
                                </div>
                                <?php }
        else {
            if  ($category_product == 'Professional') {  ?>
                                <div class="icon-text-area color-professional">
                                    <img src="<?php the_field('icon_zastosowanie_product') ?>">
                                    <h3><?php the_field('tekst_tytulu_-_zastosowanie_product') ?></h3>
                                </div>
                                <?php  } 
        }
    ?>

                                <?php the_field('opis_zastosowanie_product') ?>
                            </div>
                            <div class="desc-product-2">
                                <?php
        $terms = get_the_terms($post->ID, 'category_product');
        foreach($terms as $term) {
        $category_product = $term->name;
        }
        if  ($category_product == 'Natural') { ?>

                                <?php if( get_field('tytul_do_czego_sie_nadaje_product') ): ?>
                                <div class="icon-text-area color-natural">
                                    <img src="<?php the_field('ikona_do_czego_sie_nadaje_product') ?>">
                                    <h3><?php the_field('tytul_do_czego_sie_nadaje_product') ?></h3>
                                </div>
                                <?php endif; ?>
                                <?php }
        else {
            if  ($category_product == 'Professional') {  ?>
                                <?php if( get_field('tytul_do_czego_sie_nadaje_product') ): ?>
                                <div class="icon-text-area color-professional">
                                    <img src="<?php the_field('ikona_do_czego_sie_nadaje_product') ?>">
                                    <h3><?php the_field('tytul_do_czego_sie_nadaje_product') ?></h3>
                                </div>
                                <?php endif; ?>
                                <?php  } 
        }
    ?>

                                <?php
if( get_field('ilosc_kolumn_przycisk') ) { ?>

                                <?php if( get_field('kolumna_-_dodatkowy_opis_produkt') ): ?>
                                <div class="TwoCols-text-desc">
                                    <div class="text-left-col">
                                        <?php the_field('kolumna_-_dodatkowy_opis_produkt') ?>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <?php } else { ?>


                                <?php if( get_field('gotowe_srodki_czystosci_lewa_col') || get_field('gotowe_srodki_czystosci_prawa_col') ): ?>
                                <div class="TwoCols-text-desc">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="text-left-col">
                                                <?php the_field('gotowe_srodki_czystosci_lewa_col') ?>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="text-right-col">
                                                <?php the_field('gotowe_srodki_czystosci_prawa_col') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <?php }  ?>


                                <div class="ilosc_area_field">
                                    <?php the_field('iloscArea_col') ?>
                                </div>
                                <div class="buttons-product-area">
                                    <div class="row">
                                        <!-- <div class="col-lg-6">
                                        <a class="btn-kup-teraz"
                                            href="<?php the_field('przycisk_-_link_kupTeraz') ?>"><?php the_field('przycisk_-_tekst_kupTeraz') ?></a>
                                    </div> -->
                                        <?php
if( get_field('wlacz_przycisk_do_pobrania') ) { ?>
                                        <div class="col-lg-6">
                                            <div class="accordion" id="accordionExample">
                                                <div class="card">
                                                    <div class="card-header" id="headingOne">
                                                        <button class="btn-do-pobrania" type="button"
                                                            data-toggle="collapse" data-target="#collapseOne"
                                                            aria-expanded="true" aria-controls="collapseOne">
                                                            <?php the_field('przycisk_-_tekst_doPobrania') ?>
                                                        </button>
                                                    </div>
                                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                                        data-parent="#accordionExample">
                                                        <div class="card-body">
                                                            <ul>
                                                                <?php
                                                            if( have_rows('list-attachment-product') ):
                                                                while( have_rows('list-attachment-product') ) : the_row(); ?>

                                                                <?php if( get_sub_field('link_attachment-product') ): ?>
                                                                <li><a href="<?php the_sub_field('link_attachment-product'); ?>"
                                                                        download="<?php the_sub_field('name_attachment-product'); ?>">
                                                                        <?php the_sub_field('img-attachment-product'); ?>
                                                                        <?php the_sub_field('name_attachment-product'); ?>
                                                                    </a>
                                                                </li>
                                                                <?php endif; ?>

                                                                <?php endwhile;
                                                            else :
                                                            endif;
                                                            ?>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <?php
if( get_field('wlacz_aple_z_ikonami_produkty') ) { ?>
    <?php
    $terms = get_the_terms($post->ID, 'category_product');
        foreach($terms as $term) {
        $category_product = $term->name;
        }
        if  ($category_product == 'Natural') { ?>
    <section class="product-single-apla">
        <?php }
        else {
            if  ($category_product == 'Professional') {  ?>
        <section class="product-single-apla apla-professional">
            <?php } 
        } ?>

            <div class="container">
                <div class="row fle-center-apla-primary">
                    <?php
if( have_rows('lista_ikona_na_apli_produktu') ):
    while( have_rows('lista_ikona_na_apli_produktu') ) : the_row(); ?>

                    <div class="col-lg-4 ">
                        <div class="center-iconText-apla-product">
                            <div class="area_icons_Apla">
                                <img src="<?php the_sub_field('ikona_na_apli_produktu'); ?>">
                            </div>
                            <div class="text-icons_Apla">
                                <h3><?php the_sub_field('textPodikona_na_apli_produktu'); ?></h3>
                            </div>
                        </div>
                    </div>
                    <?php    endwhile;
                            else :
                            endif; ?>
                </div>
            </div>
        </section>
        <?php } ?>

        <!-- start new products -->
        <section class="productsNew">
            <div class="container">

                <?php
        $terms = get_the_terms($post->ID, 'category_product');
        foreach($terms as $term) {
        $category_product = $term->name;
        }
        if  ($category_product == 'Natural') { ?>
                <div class="title-section color-natural">
                    <h2><?php the_field('zobacz_cala_linie_produktu','option') ?></h2>
                </div>
                <?php }
        else {
            if  ($category_product == 'Professional') {  ?>
                <div class="title-section color-professional">
                    <h2><?php the_field('zobacz_cala_linie_produktu','option') ?></h2>
                </div>
                <?php  } 
        }
    ?>


                <!-- Swiper -->
                <div class="content-swiper-area">
                    <div class="swiper-container swiper-container-main">
                        <div class="swiper-wrapper">
                            <?php

$terms = get_the_terms($post->ID, 'category_product');
        foreach($terms as $term) {
        $category_product = $term->name;
        }
        if  ($category_product == 'Natural') { 
            $args = array(
                'post_type' => 'product',
                'category_product'=>'natural',
                'posts_per_page' => 10,
                'orderby' => 'menu_order',
                'order' => 'ASC'
             );
         }
        else {
            if  ($category_product == 'Professional') { 
                $args = array(
                    'post_type' => 'product',
                    'category_product'=>'professional',
                    'posts_per_page' => 10,
                    'orderby' => 'menu_order',
                    'order' => 'ASC'
                 );
             } 
        }

$parent = new WP_Query( $args );
if ( $parent->have_posts() ) : ?>
                            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                            <div class="swiper-slide">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="product-area">
                                        <div class="img-productArea">
                                            <?php if ( has_post_thumbnail() ) {
                                    the_post_thumbnail();
                                } ?>
                                        </div>

                                        <?php  
 if( get_field('img_product_back') ) { ?>
                                        <div class="img-productArea-back">
                                            <img src="<?php the_field('img_product_back') ?>"
                                                alt="<?php the_title(); ?>">
                                        </div>
                                        <?php } else { ?>
                                        <div class="img-productArea-back">
                                            <?php if ( has_post_thumbnail() ) {
  the_post_thumbnail();
} ?>
                                        </div>

                                        <?php }?>

                                        <div class="descProduct_item">
                                            <p><?php echo mb_strimwidth( get_the_title(), 0, 50, '...' ); ?></p>
                                            <?php the_field('iloscArea_col') ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php endwhile; ?>
                            <?php endif; 
                        wp_reset_postdata(); ?>

                        </div>

                    </div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </section>
        <!-- end new products -->
</div>

<?php get_footer() ?>