<?php 
/* 
Template Name: Strona Główna
*/ 
?>

<?php get_header() ?>

<div id="homepage" class="wrapper">
    <div class="container">
        <section class="slider">
            <div class="row no-gutters">
                <div class="col-lg-6">
                    <div class="container-img-left">
                        <a href="<?php the_field('lewa_kolumna_-_odnosnik_slider-img') ?>">
                            <div style="background-image: url(<?php the_field('bgCol-left-slider-img') ?>)"
                                class="bgCol-left-slider">
                                <div class="imgAndTxtArea">
                                    <img src="<?php the_field('lewa_kolumna_-_logo_slider-img') ?>">
                                </div>
                                <h2><?php the_field('lewa_kolumna_-_tytul_slider-img') ?></h2>
                            </div>
                            <div class="overly">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="container-img-right">
                        <a href="<?php the_field('prawa_kolumna_-_odnosnik_slider-img') ?>">
                            <div style="background-image: url(<?php the_field('bgCol-right-slider-img') ?>)" class="
                        bgCol-right-slider">
                                <div class="imgAndTxtArea">
                                    <img src="<?php the_field('prawa_kolumna_-_logo_slider-img') ?>">
                                </div>
                                <h2><?php the_field('prawa_kolumna_-_tytul_slider-img') ?></h2>
                            </div>
                            <div class="overly">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <a href="#new-products" class="arrow-down"><i class="fas fa-angle-down"></i></a>
        </section>
    </div>


    <!-- start new products -->
    <section id="new-products" class="productsNew">
        <div class="container">
            <div class="title-section">
                <h2><?php the_field('title_section_productsNew') ?></h2>
            </div>
            <div class="content-swiper-area">

                <!-- Swiper -->
                <div class="swiper-container swiper-container-main">
                    <div class="swiper-wrapper">
                        <?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'orderby' => 'menu_order',
    'order' => 'ASC'
 );
$parent = new WP_Query( $args );
if ( $parent->have_posts() ) : ?>
                        <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

                        <?php if( get_field('product_new_onHome') == 'enable_sidebar' ) { ?>

                        <div class="swiper-slide">
                            <a href="<?php the_permalink(); ?>">
                                <div class="product-area">
                                    <div class="img-productArea">
                                        <?php if ( has_post_thumbnail() ) {
                                    the_post_thumbnail();
                                } ?>
                                    </div>

                                    <?php  
 if( get_field('img_product_back') ) { ?>
                                    <div class="img-productArea-back">
                                        <img src="<?php the_field('img_product_back') ?>" alt="<?php the_title(); ?>">
                                    </div>
                                    <?php } else { ?>
                                    <div class="img-productArea-back">
                                        <?php if ( has_post_thumbnail() ) {
  the_post_thumbnail();
} ?>
                                    </div>

                                    <?php }?>

                                    <div class="descProduct_item">
                                        <p><?php echo mb_strimwidth( get_the_title(), 0, 50, '...' ); ?></p>
                                        <?php the_field('iloscArea_col') ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php } ?>
                        <?php endwhile; ?>
                        <?php endif; 
wp_reset_postdata(); ?>

                    </div>

                </div>

                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div> <!-- end content-swiper for arrow -->

        </div>
    </section>
    <!-- end new products -->



    <div class="container">
        <section class="aplaGrayHome">

            <div class="title-section-area">
                <h2><?php the_field('tytul_sekcji_aplaGrayHome') ?></h2>
            </div>
            <div class="row">
                <?php

if( have_rows('lista_ikon_i_tekstow_na_apli__aplaGrayHome') ):
    while( have_rows('lista_ikon_i_tekstow_na_apli__aplaGrayHome') ) : the_row(); ?>
                <div class="col-sm-6 col-md-3">
                    <div class="icon-areaAplaGrayHome">
                        <img src="<?php the_sub_field('ikona_aplaGrayHome') ?>">
                    </div>
                    <div class="infoDescArea">
                        <p><?php the_sub_field('opis_aplaGrayHome_kopia') ?></p>
                    </div>
                </div>
                <?php     // End loop.
    endwhile;
else :
endif; ?>

            </div>

        </section>
    </div>




    <!-- start polecane produkty -->
    <section id="new-products" class="productsNew">
        <div class="container">
            <div class="title-section">
                <h2><?php the_field('title_section_productyPolecane') ?></h2>
            </div>

            <!-- Swiper -->
            <div class="content-swiper-area">
                <div class="swiper-container swiper-container-main">
                    <div class="swiper-wrapper">
                        <?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'orderby' => 'menu_order',
    'order' => 'ASC'
 );
$parent = new WP_Query( $args );
if ( $parent->have_posts() ) : ?>
                        <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

                        <?php if( get_field('product_polecane_onHome') == 'enable_sidebar' ) { ?>

                        <div class="swiper-slide">
                            <a href="<?php the_permalink(); ?>">
                                <div class="product-area">
                                    <div class="img-productArea">
                                        <?php if ( has_post_thumbnail() ) {
                                    the_post_thumbnail();
                                } ?>
                                    </div>

                                    <?php  
 if( get_field('img_product_back') ) { ?>
                                    <div class="img-productArea-back">
                                        <img src="<?php the_field('img_product_back') ?>" alt="<?php the_title(); ?>">
                                    </div>
                                    <?php } else { ?>
                                    <div class="img-productArea-back">
                                        <?php if ( has_post_thumbnail() ) {
  the_post_thumbnail();
} ?>
                                    </div>

                                    <?php }?>

                                    <div class="descProduct_item">
                                        <p><?php echo mb_strimwidth( get_the_title(), 0, 50, '...' ); ?></p>
                                        <?php the_field('iloscArea_col') ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php } ?>
                        <?php endwhile; ?>
                        <?php endif; 
wp_reset_postdata(); ?>

                    </div>

                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </section>
    <!-- end new products -->




    <section
        style="background-image:radial-gradient(circle, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 16%, rgba(255,255,255,0.45) 45%), url(<?php the_field('tlo_sekcji_sachets') ?>)"
        class="sachets">
        <div class="container">
            <div class="title-section-area">
                <h2><?php the_field('tytul_sekcji_sachets') ?></h2>
            </div>
            <div class="list_sachets">
                <?php
                if( have_rows('list_sachet_img') ):
                    // Loop through rows.
                    while( have_rows('list_sachet_img') ) : the_row(); ?>

                <div class="sachet_itemArea">
                    <img src="<?php the_sub_field('sachet_img') ?>">
                </div>

                <?php
                endwhile;
                else :
                endif; ?>
            </div>
        </div>
    </section>

    <!-- <section class="instagram-gallery">
        <div class="container">
            <div class="title-section-area">
                <h2><?php the_field('title_instagram_gallery') ?></h2>
            </div>
            <div class="swiper-wrapper-instagram">
                <div class="swiper-container swiper-container-instagram">
                    <div id="instafeed-container" class="swiper-wrapper">
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </section> -->











</div>



































<?php get_footer() ?>