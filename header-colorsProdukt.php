<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

    <link rel="stylesheet" href="https://use.typekit.net/cao3mvv.css">


    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
    <?php wp_head(); ?>

</head>

<?php $class="custom-class"; ?>

<body <?php body_class($class); ?>>


    <?php
        $terms = get_the_terms($post->ID, 'category_product');
        foreach($terms as $term) {
        $category_product = $term->name;
        }
        if  ($category_product == 'Natural') { 
            echo '<header id="header-default" class="natural">';
         }
        else {
            if  ($category_product == 'Professional') { 
                echo '<header id="header-default" class="professional">';
             } 
        }
    ?>




    <nav class="navbar navbar-expand-xl navbar-light navbar-top fixed-top">
        <div class="container wraper-header">
            <div class="logo-wrap">





                <?php
    $terms = get_the_terms($post->ID, 'category_product');
        foreach($terms as $term) {
        $category_product = $term->name;
        }
        if  ($category_product == 'Natural') { ?>
                <a class="navbar-brand logo-mobile" href="<?php the_field('link_page_natural','option') ?>"><img
                        src="<?php the_field('logo_page_natural','option') ?>" alt="Natural"></a>
                <?php }
        else {
            if  ($category_product == 'Professional') {  ?>
                <a class="navbar-brand logo-mobile" href="<?php the_field('link_page_professional','option') ?>"><img
                        src="<?php the_field('logo_page_professional','option') ?>" alt="Professional"></a>
                <?php } 
        } ?>





            </div>







            <div class="menu-wrap">

                <!-- Collapse button burger menu Start-->
                <button class="navbar-toggler first-button" type="button" data-toggle="collapse"
                    data-target="#navbarNavDropdown" aria-controls="navbarSupportedContent20" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <div class="animated-icon1"><span></span><span></span><span></span></div>
                </button>
                <!-- Collapse button burger menu End-->

                <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'primary',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'navbarNavDropdown',
                        'menu_class'        => 'nav navbar-nav navbar-nav-primary',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker(),
                    ) );
                ?>


                <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'secondary',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'navbarNavDropdown',
                        'menu_class'        => 'nav navbar-nav navbar-nav-secondary',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker(),
                    ) );
                ?>


                <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'secondary_two',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'navbarNavDropdown',
                        'menu_class'        => 'nav navbar-nav navbar-nav-secondary-two',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker(),
                    ) );
                ?>


                <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'secondary_three',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'navbarNavDropdown',
                        'menu_class'        => 'nav navbar-nav navbar-nav-secondary-three',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker(),
                    ) );
                ?>
                <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'secondary_four',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'navbarNavDropdown',
                        'menu_class'        => 'nav navbar-nav navbar-nav-secondary-four',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker(),
                    ) );
                ?>




            </div>










        </div>
    </nav>
    </header>
    </div>