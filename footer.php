<footer class="footer">
    <div class="footer-wrapper">
        <div class="blue-bg-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 flex-center-col-footer">
                        <div class="newsletter-area">
                            <div class="title-area-newsletter">
                                <h3><?php the_field('newsletter_titleFooter','option') ?></h3>
                            </div>

                            <div class="tnp tnp-subscription">
                                <form method="post" action="<?php echo home_url(); ?>/?na=s">
                                    <input type="hidden" name="nlang" value="">
                                    <div class="form-inline-flex">
                                        <div class="tnp-field-firstname">
                                            <input placeholder="<?php the_field('newsletter_pole_imie','option') ?>"
                                                class="tnp-name form-control" type="text" name="nn" value="">
                                        </div>
                                        <div class="tnp-field-email">
                                            <input placeholder="<?php the_field('newsletter_pole_email','option') ?>"
                                                class="tnp-email form-control" type="email" name="ne" value="" required>
                                        </div>
                                        <div class="btn-send-newsletter"><input class="tnp-submit" type="submit"
                                                value="<?php the_field('newsletter_button_name','option') ?>">
                                        </div>

                                    </div>
                                    <div class="tnp-field"><label><input type="checkbox" name="ny" required
                                                class="tnp-privacy"> <?php the_field('newsletter_info_footer','option') ?></label>
                                    </div>

                                </form>
                            </div>


                        </div>
                    </div>
                    <div class="col-lg-3 flex-center-col-footer padding-col-socialMediaFooter">
                        <div class="icon-socialmedia-footerArea">
                            <?php
if( have_rows('ikony_social_media_foofter_list','option') ):
    while( have_rows('ikony_social_media_foofter_list','option') ) : the_row(); ?>
                            <a
                                href="<?php the_sub_field('ikona_link_social_media_foofter','option'); ?>"><?php the_sub_field('ikona_social_media_foofter','option'); ?></a>
                            <?php    endwhile;
else :
 endif; ?>
                        </div>
                        <div class="numberAndmailAreaFooter">
                            <?php the_field('numery_telefonow_i_mail_footer','option') ?>
                        </div>
                    </div>
                    <div class="col-lg-3 flex-center-col-footer">
                        <div class="Sitemap_footer">
                            <?php
if( have_rows('mapa_strony_footer','option') ):
    while( have_rows('mapa_strony_footer','option') ) : the_row(); ?>
                            <a
                                href="<?php the_sub_field('link_strony_mapa_strony_footer','option'); ?>"><?php the_sub_field('nazwa_strony_mapa_strony_footer','option'); ?></a>
                            <?php    endwhile;
else :
 endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row flex-center-footerInfo">
                <div class="col-md-6">
                    <div class="left-col-info">
                        <p>Realization <a href="https://redrocks.pl">RedRockS &copy;</a> <?php echo date("Y"); ?></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="right-col-info">
                        <p><?php the_field('info_o_stronie_prawa_kolumna','option') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>



<!-- Add instagram slider image - narazie wyłaczamy do późnij -->
<!-- <script src="https://cdn.jsdelivr.net/gh/stevenschobert/instafeed.js@2.0.0rc1/src/instafeed.min.js"></script> -->


<!-- Swiper  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.5.0/swiper-bundle.min.js"></script>




<?php wp_footer(); ?>


</body>

</html>