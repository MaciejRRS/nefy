const gulp = require('gulp');
// minify JS
const uglify = require('gulp-uglify');
// convert scss to css
const sass = require('gulp-sass')
// rename extention
const rename = require('gulp-rename');
// delite files
const del = require('del');
// add sourcemaps to min.css
const sourcemaps = require('gulp-sourcemaps');
// concat all scripts to main.js
const concat = require('gulp-concat');
// browserSync 
const browserSync = require('browser-sync').create();

const scssWatcher = gulp.watch(['assets/src/sass/*.scss']);
const phpWatcher = gulp.watch(['./*.php'])
const scriptsWatcher = gulp.watch(['assets/src/js/custom/*.js'])


function styles() {
    return gulp.src(['assets/src/bootstrap/bootstrap.scss', 'assets/src/sass/main.scss'])
        .pipe(sass({ outputStyle: 'compressed', }).on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('assets/dist/css'))
}


function get_js_libr() {
    return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js', 'node_modules/popper.js/dist/popper.min.js'])
        .pipe(gulp.dest("assets/src/js/libr"))
}


function clean() {
    return del(['assets/dist/',])
}

function watch() {
    browserSync.init({ proxy: 'http://nefy.test', online: true })
    scssWatcher.on('change', gulp.series(clean, styles, browserSync.reload))
    phpWatcher.on('change', browserSync.reload)
    scriptsWatcher.on('change', browserSync.reload)
}

get_js_libr();
exports.default = gulp.series(clean, styles, watch)
