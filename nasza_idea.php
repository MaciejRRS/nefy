<?php 
/* 
Template Name: Nasza idea
*/ 
?>

<?php get_header() ?>

<div id="subpageDefault">
    <div class="subpage-wrapper">
        <div class="container">

            <?php if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
    } ?>

            <div class="title-section">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="idea-content-cols">
                <?php
if( have_rows('nasza_idea_-_lista_blokow') ):
    while( have_rows('nasza_idea_-_lista_blokow') ) : the_row(); ?>
                <div id="<?php the_sub_field('id_for_anchor') ?>" class="idea-content nth-cols">
                    <div class="row">
                        <div class="col-lg-6 col-idea-order-one">
                            <div class="bg-cover-idea">

                                <?php $image_id = get_sub_field( 'zdjecie_bloku_nasza_idea', false ); ?>

                                <div class="img-cover-idea <?php the_sub_field('wybierz_color_bloku_idea') ?>">


                                    <img src="<?php echo wp_get_attachment_image_src( $image_id, 'large' )[0]; ?>"
                                        alt="">
                                </div>


                            </div>
                        </div>
                        <div class="col-lg-6 col-idea-order-two">
                            <div class="subtitle-pag_idea">
                                <h2><?php the_field('podtytul_strony_nasza_idea') ?></h2>
                            </div>
                            <div class="text-idea">
                                <h3><?php the_sub_field('tytul_bloku_nasza_idea') ?></h3>
                                <?php the_sub_field('opis_bloku_nasza_idea') ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php 
endwhile;
else :
endif; ?>

            </div>
        </div>
    </div>
</div>



































<?php get_footer() ?>