<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( $query->have_posts() )
{
	?>
<div class="row">
    <?php
	while ($query->have_posts())
	{
		$query->the_post();
		
        ?>
    <?php
        $terms = get_the_terms($post->ID, 'category_recipes');
        foreach($terms as $term) {
        $category_recipes = $term->name;
    ?>

    <div class="col-md-6">
        <a href="<?php the_permalink(); ?>">
            <div class="poradnik-post-area">

                <div class="img-productArea"
                    style="background-image: url(<?php the_post_thumbnail_url('thumbnail'); ?>)"></div>

                <div class="descProduct_item">
                    <?php 
                    $title_recipes = get_the_title();
                    if  ($category_recipes == 'Natural') {
                        echo '<h3 class="natural_color_line">'.get_the_title().'</h3>';
                    }
                    else {
                        if  ($category_recipes == 'Professional') {
                            echo '<h3 class="professional_color_line">'.get_the_title().'</h3>';
                        } 
                    }
                ?>
                    <div class="descApla">
                        <p><?php echo mb_strimwidth( get_the_excerpt(), 0, 126, '...' ); ?></p>
                    </div>
                    <img src="<?php the_field('miniatura_kategorii_custom_post_natural_professional', $term); ?>" />
                    <?php } ?>
                </div>
            </div>
        </a>
    </div>
    <?php
	}
	?>
</div>


<div class="pagination">

    <div class="nav-previous"><?php next_posts_link( '<i class="fas fa-angle-down"></i>', $query->max_num_pages ); ?>
    </div>
    <div class="nav-next"><?php previous_posts_link( '<i class="fas fa-angle-up"></i>' ); ?></div>
    <?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
</div>
<?php
}
else
{
	echo "";
}
?>