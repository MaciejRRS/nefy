<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>


<div class="row">
    <?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?>
    <div class="col-md-4">

        <a href="<?php the_permalink(); ?>">
            <div class="product-area">
                <div class="img-productArea">
                    <?php if ( has_post_thumbnail() ) {
                                    the_post_thumbnail();
                                } ?>
                </div>

                <?php  
 if( get_field('img_product_back') ) { ?>
                <div class="img-productArea-back">
                    <img src="<?php the_field('img_product_back') ?>" alt="<?php the_title(); ?>">
                </div>
                <?php } else { ?>
                <div class="img-productArea-back">
                    <?php if ( has_post_thumbnail() ) {
  the_post_thumbnail();
} ?>
                </div>

                <?php }?>

                <div class="descProduct_item">
                    <p><?php echo mb_strimwidth( get_the_title(), 0, 50, '...' ); ?></p>
                    <?php the_field('iloscArea_col') ?>
                </div>
            </div>
        </a>

    </div>


    <?php
	}
	?>
</div>
<!-- Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br /> -->

<div class="pagination">

    <div class="nav-previous"><?php next_posts_link( '<i class="fas fa-angle-down"></i>', $query->max_num_pages ); ?>
    </div>
    <div class="nav-next"><?php previous_posts_link( '<i class="fas fa-angle-up"></i>' ); ?></div>
    <?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
</div>
<?php
}
else
{
	echo "";
}
?>